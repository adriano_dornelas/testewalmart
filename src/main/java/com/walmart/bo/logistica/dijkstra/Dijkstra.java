package com.walmart.bo.logistica.dijkstra;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Dijkstra {

    //Lista que guarda os vertices pertencentes ao menor caminho encontrado
    List<Vertice> menorCaminho;
    
    //Variavel que recebe os verrtices pertencentes ao menor caminho
    Vertice verticeCaminho;
    
    //Variavel que guarda o vertice que esta sendo visitado 
    Vertice atual;
    
    //Variavel que marca o vizinho do vertice atualmente visitado 
    Vertice vizinho;
    
    //Corte de vertices que ja tiveram suas distancias marcadas e cujos vizinhos não foram visitados
    List<Vertice> fronteira;
    
    //Guarda o número de vertices não visitados
    int verticesNaoVisitados;

    //
    public List<Vertice> encontrarMenorCaminho(Grafo grafo, Vertice v1, Vertice v2) {
        menorCaminho = new ArrayList<Vertice>();
        fronteira = new ArrayList<Vertice>();

        //No início, todos os vertices do grafo não foram visitados
	    verticesNaoVisitados = grafo.getVertices().size();
            
        //O primeiro  nó a ser visitado é da origem do caminho
        atual = v1;
        //Adiciona o primeiro nó no corte
        fronteira.add(atual);
        //Adiciona a origem na lista do menor caminho
        menorCaminho.add(atual);
            
        //Inicializa
        for (int i=0; i < grafo.getVertices().size(); i++) {
        	Vertice v = grafo.getVertices().get(i); 
        	
            //O atual tem distancia zero, e todos os outros, 99999 (infinita)
            if (v.getDescricao().equals(atual.getDescricao())){
                v.setDistancia(0);
            } else{
                v.setDistancia(99999);
            }
            
            v.setPai(null);
            v.setVisitado(false);
        }
        
        //O algoritmo continua ate que todos os vertices relacionados sejam visitados 
        while ((verticesNaoVisitados != 0) && (this.fronteira.size() > 0)) {
            
            //Toma-se sempre o vertice com menor distancia, que é o primeiro da lista do corte
            atual = this.fronteira.get(0);
            
            /*Para cada vizinho (cada aresta), calcula-se a sua possivel distancia,
            somando a distancia do vertice atual com a da aresta correspondente.
            Se essa distancia for menor que a distancia do vizinho, esta é atualizada.
            */ 
            for (int i=0; i < atual.getArestas().size(); i++) {
                vizinho = atual.getArestas().get(i).getDestino();                               
                if (!vizinho.isVisitado()){
                    //Comparando a dist�ncia do vizinho com a poss�vel dist�ncia
                    if (vizinho.getDistancia() > (atual.getDistancia() + atual.getArestas().get(i).getPeso())) {
                        vizinho.setDistancia(atual.getDistancia() + atual.getArestas().get(i).getPeso());
                    	vizinho.setPai(atual);
                        
                        /*Se o vizinho é o vertice procurado, e foi feita uma mudança na distancia,
                         * a lista com o menor caminho anterior é apagada, pois existe um caminho menor ainda.
                         * Cria-se a nova lista do menor caminho, com os vertices pais, até o vértice origem.
                         * */
                        if (vizinho == v2){
                            menorCaminho.clear();
                            verticeCaminho = vizinho;
                            menorCaminho.add(vizinho);
                            while (verticeCaminho.getPai() != null){
                                menorCaminho.add(verticeCaminho.getPai());
                                verticeCaminho = verticeCaminho.getPai();
                            }
                            //Ordena a lista do menor caminho, para que ele seja exibido da origem ao destino.
                            Collections.sort(menorCaminho);
                        }
                    }
                    //Cada vizinho, depois de visitado, é adicionado ao corte
                    if (!this.fronteira.contains(vizinho))
                    	this.fronteira.add(vizinho);
                }
            }
            
            //Marca o vértice atual como visitado e o retira do corte
            atual.setVisitado(true);
            verticesNaoVisitados--;
            this.fronteira.remove(atual);
            
            /*Ordena a lista do corte, para que o vértice com menor distancia fique na primeira
             * posição*/            
            Collections.sort(fronteira);
        }
                        
        return menorCaminho;
    }
	
}