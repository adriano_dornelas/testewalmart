package com.walmart.bo.logistica.dijkstra;

import java.util.ArrayList;
import java.util.List;

public class Vertice implements Comparable<Vertice> {

	private String descricao;
	private int distancia;
	private boolean visitado = false;
	private Vertice pai;
	private List<Aresta> arestas = new ArrayList<Aresta>();
	private List<Vertice> vizinhos = new ArrayList<Vertice>();

	public Vertice() {
		super();
	}

	public Vertice(String descricao) {
		super();
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public int getDistancia() {
		return distancia;
	}
	
	public void setDistancia(int distancia) {
		this.distancia = distancia;
	}
	
	public boolean isVisitado() {
		return visitado;
	}
	
	public void setVisitado(boolean visitado) {
		this.visitado = visitado;
	}
	
	public Vertice getPai() {
		return pai;
	}
	
	public void setPai(Vertice pai) {
		this.pai = pai;
	}
	
	public List<Aresta> getArestas() {
		return arestas;
	}
	
	public void setArestas(List<Aresta> arestas) {
		this.arestas = arestas;
	}
	
	public List<Vertice> getVizinhos() {
		return vizinhos;
	}
	
	public void setVizinhos(List<Vertice> vizinhos) {
		this.vizinhos = vizinhos;
	}

	public void addVizinho(Vertice vizinho) {
		this.vizinhos.add(vizinho);
	}
	
	public void addAresta(Aresta aresta) {
		this.arestas.add(aresta);
	}
	
	@Override
	public int compareTo(Vertice o) {
		if (this.getDistancia() < o.getDistancia()) return -1;
        else if (this.getDistancia() == o.getDistancia()) return 0;
        else return 1;	
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertice other = (Vertice) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.descricao;
	}
	
}
