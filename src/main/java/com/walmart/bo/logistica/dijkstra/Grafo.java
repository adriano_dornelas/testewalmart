package com.walmart.bo.logistica.dijkstra;

import java.util.ArrayList;
import java.util.List;

public class Grafo {

	private List<Vertice> vertices = new ArrayList<Vertice>();
	private List<Aresta> arestas = new ArrayList<Aresta>();

	public List<Vertice> getVertices() {
		return vertices;
	}
 
	public void setVertices(List<Vertice> vertices) {
		this.vertices.addAll(vertices);
	}
	
	
	public List<Aresta> getArestas() {
		return arestas;
	}

	public void setArestas(List<Aresta> arestas) {
		this.arestas.addAll(arestas);
	}

	public Vertice findVertice(String descricao){
        for (int i=0; i < this.getVertices().size(); i++) {
            if (descricao.equalsIgnoreCase(this.getVertices().get(i).getDescricao())) {
            	return this.getVertices().get(i);
            }
        }
        return null;
	}
	
}
