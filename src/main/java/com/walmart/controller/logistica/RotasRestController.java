package com.walmart.controller.logistica;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.walmart.dto.logistica.MalhaDto;
import com.walmart.dto.logistica.RotaDto;
import com.walmart.service.logistica.interfaces.IMapaService;

@RestController
public class RotasRestController {
	
	@Autowired private IMapaService mapaService;

	@RequestMapping(value="/adicionaMapa", method=RequestMethod.POST)
	public String adicionaMapa(@RequestHeader String nome, @RequestBody List<MalhaDto> malhas) {
		mapaService.adicionarMapa(nome, malhas);
		return "Mapa adicionado com sucesso";
	}
	
	@RequestMapping(value="/pesquisaRota")
	public RotaDto pesquisaRota(@RequestParam String nomeMapa, @RequestParam String origem, 
			@RequestParam String destino, @RequestParam Double autonomia, @RequestParam Double precoCombustivel) {
		return mapaService.pesquisarRota(nomeMapa, origem, destino, autonomia, precoCombustivel);
	}
	
}	
