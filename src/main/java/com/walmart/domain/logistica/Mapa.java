package com.walmart.domain.logistica;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

import com.walmart.enumerators.logistica.MapaStatus;

@Entity
public class Mapa {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column
	@NotBlank
	private String nome;
	
	@Enumerated(EnumType.STRING)
	private MapaStatus status = MapaStatus.A;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public MapaStatus getStatus() {
		return status;
	}

	public void setStatus(MapaStatus status) {
		this.status = status;
	}

}
