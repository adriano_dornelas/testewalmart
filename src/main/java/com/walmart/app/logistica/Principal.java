package com.walmart.app.logistica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan({"com.walmart"})
@EnableAutoConfiguration
@EnableTransactionManagement
@ImportResource("classpath:spring-config.xml")
public class Principal {

	public static void main(String[] args) {
		SpringApplication.run(Principal.class, args);
	}

}
