package com.walmart.enumerators.logistica;

public enum MapaStatus {

	A("Ativo"),
	I("Inativo");
	
	private final String descricao;

	private MapaStatus(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
	
}
