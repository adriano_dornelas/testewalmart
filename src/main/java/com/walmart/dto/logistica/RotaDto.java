package com.walmart.dto.logistica;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RotaDto {

	private List<String> cidades = new ArrayList<String>();
	private Double distancia;
	private BigDecimal custo;

	public List<String> getCidades() {
		return cidades;
	}
	
	public void setCidades(List<String> cidades) {
		this.cidades = cidades;
	}
	
	public Double getDistancia() {
		return distancia;
	}

	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}

	public BigDecimal getCusto() {
		return custo;
	}
	
	public void setCusto(BigDecimal custo) {
		this.custo = custo;
	}
	
}
