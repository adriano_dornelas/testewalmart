package com.walmart.dto.logistica;

public class MalhaDto {

	private String origem;
	private String destino;
	private Integer distancia;
	
	public MalhaDto() {
		super();
	}

	public MalhaDto(String origem, String destino, Integer distancia) {
		super();
		this.origem = origem;
		this.destino = destino;
		this.distancia = distancia;
	}

	public String getOrigem() {
		return origem;
	}
	
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	
	public String getDestino() {
		return destino;
	}
	
	public void setDestino(String destino) {
		this.destino = destino;
	}
	
	public Integer getDistancia() {
		return distancia;
	}
	
	public void setDistancia(Integer distancia) {
		this.distancia = distancia;
	}
	
}
