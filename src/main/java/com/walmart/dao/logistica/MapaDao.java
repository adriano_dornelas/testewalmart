package com.walmart.dao.logistica;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.walmart.dao.GenericDao;
import com.walmart.dao.logistica.interfaces.IMapaDao;
import com.walmart.domain.logistica.Mapa;
import com.walmart.enumerators.logistica.MapaStatus;

@Repository
public class MapaDao extends GenericDao<Mapa> implements IMapaDao {

	@SuppressWarnings("unchecked")
	@Override
	public Mapa findByNome(String nome) {
		String hql = "from Mapa "
				+ "where nome = :nome "
				+ "and status = :status";
		Query query = getSession().createQuery(hql);
		query.setParameter("nome", nome);
		query.setParameter("status", MapaStatus.A);
		List<Mapa> lista = query.setReadOnly(true).list(); 
		return lista.isEmpty() ? null : (Mapa) lista.get(0);
	}
	
}
