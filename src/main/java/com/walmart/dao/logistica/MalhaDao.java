package com.walmart.dao.logistica;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.walmart.dao.GenericDao;
import com.walmart.dao.logistica.interfaces.IMalhaDao;
import com.walmart.domain.logistica.Malha;

@Repository
public class MalhaDao extends GenericDao<Malha> implements IMalhaDao {

	@SuppressWarnings("unchecked")
	@Override
	public Set<String> getCidades(Integer idMapa) {
		Set<String> cidades = new HashSet<String>();
		String hql = "select distinct mlh.origem "
				+ "from Malha mlh "
				+ "where mlh.mapa.id = :idMapa";
		Query query = getSession().createQuery(hql);
		query.setParameter("idMapa", idMapa);
		cidades.addAll(query.setReadOnly(true).list());
		
		hql = "select distinct mlh.destino "
				+ "from Malha mlh "
				+ "where mlh.mapa.id = :idMapa";
		query = getSession().createQuery(hql);
		query.setParameter("idMapa", idMapa);
		cidades.addAll(query.setReadOnly(true).list());
		
		return cidades;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Malha> getMalhas(Integer idMapa) {
		String hql = "from Malha "
				+ "where mapa.id = :idMapa";
		Query query = getSession().createQuery(hql);
		query.setParameter("idMapa", idMapa);
		return query.setReadOnly(true).list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public Malha getMalhaByOrigemDestino(Integer idMapa, String origem,
			String destino) {
		String hql = "from Malha "
				+ "where mapa.id = :idMapa "
				+ "and origem = :origem "
				+ "and destino = :destino";
		Query query = getSession().createQuery(hql);
		query.setParameter("idMapa", idMapa);
		query.setParameter("origem", origem);
		query.setParameter("destino", destino);
		List<Malha> lista = query.setReadOnly(true).list();
		return lista.isEmpty() ? null : (Malha) lista.get(0); 
	}
	
}
