package com.walmart.dao.logistica.interfaces;

import java.util.List;
import java.util.Set;

import com.walmart.dao.interfaces.IGenericDao;
import com.walmart.domain.logistica.Malha;

public interface IMalhaDao extends IGenericDao<Malha> {

	Set<String> getCidades(Integer idMapa);
	List<Malha> getMalhas(Integer idMapa);
	Malha getMalhaByOrigemDestino(Integer idMapa, String origem, String destino);
	
}
