package com.walmart.dao.logistica.interfaces;

import com.walmart.dao.interfaces.IGenericDao;
import com.walmart.domain.logistica.Mapa;

public interface IMapaDao extends IGenericDao<Mapa> {

	Mapa findByNome(String nome);
	
}
 