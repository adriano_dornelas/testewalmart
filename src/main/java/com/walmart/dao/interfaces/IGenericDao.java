package com.walmart.dao.interfaces;

import org.hibernate.Session;

public interface IGenericDao<T> {

	Session getSession();
	void salvar(T object);
	void excluir(T object);
	T getById(Integer id);
	
}
