package com.walmart.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.walmart.dao.interfaces.IGenericDao;

public abstract class GenericDao<T> implements IGenericDao<T> {

	Class<T> type;
	
	@Autowired SessionFactory sessionFactory;
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public void salvar(T object) {
		getSession().saveOrUpdate(object);
	}

	@Override
	public void excluir(T object) {		
		getSession().delete(object);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getById(Integer id) {
		return (T) getSession().load(type, id);
	}
	
}
