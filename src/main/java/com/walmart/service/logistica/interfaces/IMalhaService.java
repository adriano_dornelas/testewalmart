package com.walmart.service.logistica.interfaces;

import java.util.List;
import java.util.Set;

import com.walmart.domain.logistica.Malha;

public interface IMalhaService {

	void salvar(Malha malha);
	void excluir(Integer id);
	Set<String> getCidades(Integer idMapa);
	List<Malha> getMalhas(Integer idMapa);
	Malha getMalhaByOrigemDestino(Integer idMapa, String origem, String destino);
	
}