package com.walmart.service.logistica.interfaces;

import java.util.List;

import com.walmart.domain.logistica.Mapa;
import com.walmart.dto.logistica.MalhaDto;
import com.walmart.dto.logistica.RotaDto;

public interface IMapaService {

	void salvar(Mapa mapa);
	void excluir(Integer id);
	void adicionarMapa(String nome, List<MalhaDto> malhas);
	RotaDto pesquisarRota(String nomeMapa, String origem, String destino, Double autonomia, Double precoCombustivel);
	
}
