package com.walmart.service.logistica;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.walmart.bo.logistica.dijkstra.Aresta;
import com.walmart.bo.logistica.dijkstra.Dijkstra;
import com.walmart.bo.logistica.dijkstra.Grafo;
import com.walmart.bo.logistica.dijkstra.Vertice;
import com.walmart.dao.logistica.interfaces.IMapaDao;
import com.walmart.domain.logistica.Malha;
import com.walmart.domain.logistica.Mapa;
import com.walmart.dto.logistica.MalhaDto;
import com.walmart.dto.logistica.RotaDto;
import com.walmart.service.logistica.interfaces.IMalhaService;
import com.walmart.service.logistica.interfaces.IMapaService;

@Service
public class MapaService implements IMapaService {

	@Autowired private IMapaDao dao;
	@Autowired private IMalhaService malhaService;
	
	@Override
	@Transactional
	public void salvar(Mapa mapa) {
		dao.salvar(mapa);
	}

	@Override
	@Transactional
	public void excluir(Integer id) {
		Mapa mapa = dao.getById(id);
		dao.excluir(mapa);
	}

	@Override
	@Transactional
	public void adicionarMapa(String nome, List<MalhaDto> malhas) {
		//Pesquisa se mapa já existe
		Mapa mapa = dao.findByNome(nome);
		boolean novoMapa = (mapa == null); 
		
		//Se não achou, insere novo
		if (novoMapa) {
			mapa = new Mapa();
			mapa.setNome(nome);
			dao.salvar(mapa);
		}
		
		//Insere malhas
		for (MalhaDto malhaDto : malhas) {
			Malha malha = null;
			
			//Pesquisa se malha já existe (quando não for novo mapa)
			if (!novoMapa)
				malha = malhaService.getMalhaByOrigemDestino(mapa.getId(), malhaDto.getOrigem(), malhaDto.getDestino());
			
			//Se não existe, insere nova malha
			if (malha == null) {
				malha = new Malha();
				malha.setMapa(mapa);
				malha.setOrigem(malhaDto.getOrigem());
				malha.setDestino(malhaDto.getDestino());
				malha.setDistancia(malhaDto.getDistancia());
			}
			//Se existe, atualiza
			else {
				malha.setDistancia(malhaDto.getDistancia());
			}
			
			malhaService.salvar(malha);
		}
	}

	@Override
	@Transactional(readOnly=true)
	public RotaDto pesquisarRota(String nomeMapa, String origem,
			String destino, Double autonomia, Double precoCombustivel) {
		RotaDto rotaDto = new RotaDto();
		
		//Localiza mapa pelo nome
		Mapa mapa = dao.findByNome(nomeMapa);
		if (mapa == null) {
			throw new RuntimeException("Mapa "+nomeMapa+" não encontrado.");
		}
		 
		//Pesquisa cidades do mapa
		Set<String> cidades = malhaService.getCidades(mapa.getId());
		
		//Pesquisa malhas
		List<Malha> malhas = malhaService.getMalhas(mapa.getId());
		
		//Aplica algoritmo de Dijkstra
		//Cria vertices
		Map<String, Vertice> vertices = new HashMap<String, Vertice>();
		for (String cidade : cidades) {
			vertices.put(cidade, new Vertice(cidade));
		}

		//Cria arestas
		List<Aresta> arestas = new ArrayList<Aresta>();
		for (Malha malha : malhas) {
			Vertice v1 = vertices.get(malha.getOrigem());
			Vertice v2 = vertices.get(malha.getDestino());
			
			Aresta a = new Aresta(v1, v2, malha.getDistancia());
			v1.addVizinho(v2);
			v1.addAresta(a);
			arestas.add(a);

			a = new Aresta(v2, v1, malha.getDistancia());
			v2.addVizinho(v1);
			v2.addAresta(a);
			arestas.add(a);
		}
		
		//Alimenta grafo
		Grafo grafo = new Grafo();
		grafo.setVertices(new ArrayList<Vertice>(vertices.values()));
		grafo.setArestas(arestas);
		
		//Encontra menor caminho
		Dijkstra dijkstra = new Dijkstra();
		Vertice v1 = grafo.findVertice(origem);
		Vertice v2 = grafo.findVertice(destino);
		List<Vertice> menorCaminho = dijkstra.encontrarMenorCaminho(grafo, v1, v2);

		if (v2.getDistancia() != 99999) {
			for (Vertice vertice : menorCaminho) {
				rotaDto.getCidades().add(vertice.getDescricao());
			}
			
			rotaDto.setDistancia(new Double(v2.getDistancia()));
			
			BigDecimal custo = new BigDecimal((v2.getDistancia() / autonomia) * (precoCombustivel));
			rotaDto.setCusto(custo.setScale(2, RoundingMode.HALF_UP));
		}
		
		return rotaDto;
	}

}
