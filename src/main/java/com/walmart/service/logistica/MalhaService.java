package com.walmart.service.logistica;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.walmart.dao.logistica.interfaces.IMalhaDao;
import com.walmart.domain.logistica.Malha;
import com.walmart.service.logistica.interfaces.IMalhaService;

@Service
public class MalhaService implements IMalhaService {

	@Autowired private IMalhaDao dao;
	
	@Override
	@Transactional
	public void salvar(Malha malha) {
		dao.salvar(malha);
	}

	@Override
	@Transactional
	public void excluir(Integer id) {
		Malha malha = dao.getById(id);
		dao.excluir(malha);
	}

	@Override
	@Transactional(readOnly=true)
	public Set<String> getCidades(Integer idMapa) {
		return dao.getCidades(idMapa);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Malha> getMalhas(Integer idMapa) {
		return dao.getMalhas(idMapa);
	}

	@Override
	@Transactional(readOnly=true)
	public Malha getMalhaByOrigemDestino(Integer idMapa, String origem,
			String destino) {
		return dao.getMalhaByOrigemDestino(idMapa, origem, destino);
	}

}
