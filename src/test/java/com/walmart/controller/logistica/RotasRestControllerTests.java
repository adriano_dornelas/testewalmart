package com.walmart.controller.logistica;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

import com.walmart.app.logistica.Principal;
import com.walmart.dto.logistica.MalhaDto;
import com.walmart.dto.logistica.RotaDto;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Principal.class)
public class RotasRestControllerTests {
	
	@Autowired RotasRestController rotasRestController;

	@Test
	public void testAdicionaMapa() {
		List<MalhaDto> malhas = new ArrayList<MalhaDto>();
		malhas.add(new MalhaDto("Sao Paulo", "Campinas", 93));
		malhas.add(new MalhaDto("Sao Paulo", "Sorocaba", 99));
		malhas.add(new MalhaDto("Sorocaba", "Botucatu", 156));
		malhas.add(new MalhaDto("Ourinhos", "Botucatu", 187));
		malhas.add(new MalhaDto("Campinas", "Sao Carlos", 145));
		malhas.add(new MalhaDto("Bauru", "Sao Carlos", 147));
		malhas.add(new MalhaDto("Bauru", "Botucatu", 93));
		malhas.add(new MalhaDto("Bauru", "Marilia", 106));
		malhas.add(new MalhaDto("Assis", "Marilia", 79));
		malhas.add(new MalhaDto("Assis", "Presidente Prudente", 125));
		malhas.add(new MalhaDto("Assis", "Ourinhos", 70));
		malhas.add(new MalhaDto("Ourinhos", "Marilia", 92));
		malhas.add(new MalhaDto("Campinas", "Ribeirao Preto", 226));
		malhas.add(new MalhaDto("Sao Carlos", "Ribeirao Preto", 99));
		malhas.add(new MalhaDto("Dracena", "Presidente Prudente", 99));
		malhas.add(new MalhaDto("Sorocaba", "Campinas", 86));
		
		rotasRestController.adicionaMapa("Teste", malhas);
	}
	
	@Test
	public void testPesquisaRota() {
		RotaDto rotaDto = rotasRestController.pesquisaRota("Teste", "Sao Paulo", "Presidente Prudente", new Double(10), new Double(2.91));
		assertTrue(rotaDto.getDistancia().intValue() == 637);
		assertTrue(rotaDto.getCusto().doubleValue() == 185.37);

		rotaDto = rotasRestController.pesquisaRota("Teste", "Sao Paulo", "Assis", new Double(10), new Double(2.91));
		assertTrue(rotaDto.getDistancia().intValue() == 512);
		assertTrue(rotaDto.getCusto().doubleValue() == 148.99);

		rotaDto = rotasRestController.pesquisaRota("Teste", "Sorocaba", "Sao Carlos", new Double(10), new Double(2.91));
		assertTrue(rotaDto.getDistancia().intValue() == 231);
		assertTrue(rotaDto.getCusto().doubleValue() == 67.22);

		rotaDto = rotasRestController.pesquisaRota("Teste", "Assis", "Sao Carlos", new Double(10), new Double(2.91));
		assertTrue(rotaDto.getDistancia().intValue() == 426);
		assertTrue(rotaDto.getCusto().doubleValue() == 123.97);
	}

}
