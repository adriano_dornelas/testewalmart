package com.walmart.bo.logistica.dijkstra;

import java.util.ArrayList;

import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;

public class DijkstraTests {

	@Test
	public void test() {
		Vertice va = new Vertice("A");
		Vertice vb = new Vertice("B");
		Vertice vc = new Vertice("C");
		Vertice vd = new Vertice("D");
		Vertice ve = new Vertice("E");
		
		Aresta ab = new Aresta(va, vb, 10);
		va.addVizinho(vb);
		va.addAresta(ab);
		
		Aresta bd = new Aresta(vb, vd, 15);
		vb.addVizinho(vd);
		vb.addAresta(bd);
		
		Aresta ac = new Aresta(va, vc, 20);
		va.addVizinho(vc);
		va.addAresta(ac);
		
		Aresta cd = new Aresta(vc, vd, 30);
		vc.addVizinho(vd);
		vc.addAresta(cd);
		
		Aresta be = new Aresta(vb, ve, 50);
		vb.addVizinho(ve);
		vb.addAresta(be);
		
		Aresta de = new Aresta(vd, ve, 30);
		vd.addVizinho(ve);
		vd.addAresta(de);
		
		List<Vertice> vertices = new ArrayList<Vertice>();
		vertices.add(va);
		vertices.add(vb);
		vertices.add(vc);
		vertices.add(vd);
		vertices.add(ve);
		
		List<Aresta> arestas = new ArrayList<Aresta>();
		arestas.add(ab);
		arestas.add(bd);
		arestas.add(ac);
		arestas.add(cd);
		arestas.add(be);
		arestas.add(de);
		
		Grafo grafo = new Grafo();
		grafo.setVertices(vertices);
		grafo.setArestas(arestas);
		
		List<Vertice> menorCaminho = new ArrayList<Vertice>();
        
        Dijkstra dijkstra = new Dijkstra();
        
        //Testa combinações
        Vertice v1 = grafo.findVertice("A");
        Vertice v2 = grafo.findVertice("D");
        menorCaminho = dijkstra.encontrarMenorCaminho(grafo, v1, v2);
        
        String caminho = "";
        for (Vertice vertice : menorCaminho) {
			caminho += vertice.getDescricao() + ",";
		}
        assertTrue("Falhou menor caminho entre A e D",caminho.equals("A,B,D,"));

        //
        v1 = grafo.findVertice("A");
        v2 = grafo.findVertice("E");
        menorCaminho = dijkstra.encontrarMenorCaminho(grafo, v1, v2);
        
        caminho = "";
        for (Vertice vertice : menorCaminho) {
			caminho += vertice.getDescricao() + ",";
		}
        assertTrue("Falhou menor caminho entre A e E",caminho.equals("A,B,D,E,"));
        
        //
        v1 = grafo.findVertice("B");
        v2 = grafo.findVertice("E");
        menorCaminho = dijkstra.encontrarMenorCaminho(grafo, v1, v2);
        
        caminho = "";
        for (Vertice vertice : menorCaminho) {
			caminho += vertice.getDescricao() + ",";
		}
        assertTrue("Falhou menor caminho entre B e E",caminho.equals("B,D,E,"));

        //
        v1 = grafo.findVertice("C");
        v2 = grafo.findVertice("E");
        menorCaminho = dijkstra.encontrarMenorCaminho(grafo, v1, v2);
        
        caminho = "";
        for (Vertice vertice : menorCaminho) {
			caminho += vertice.getDescricao() + ",";
		}
        assertTrue("Falhou menor caminho entre C e E",caminho.equals("C,D,E,"));
	}

}
